---
layout: handbook-page-toc
title: "Customer Insight Page"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Customer Case Studies
Read the current GitLab customer case studies on the [GitLab customer page](https://about.gitlab.com/customers/).

#### Common Interview questions
Interview Questions: (Select the questions we should ask)

**Why GitLab**
- [ ] What insights do you have that might make a good case study today?
- [ ] Describe what your organization does, how the software is used, and how your team helps solve business challenges.
- [ ] What problem were you trying to solve when you were looking at GitLab?
- [ ] What was your process before using GitLab?
- [ ] Why did you choose to replace your current tooling?
- [ ] What were the negative consequences of your previous environment?
- [ ] What would have happened if you hadn't replaced your tooling
- [ ] How was this problem affecting you?
- [ ] What products were you using before using GitLab?
- [ ] What were the required capabilities you were looking for?
- [ ] Why did you choose GitLab?

**What if**
- [ ] What would have happened if you hadn't selected GitLab?

**Feedback on GitLab**
- [ ] How did GitLab solve those problems you were suffering from?
- [ ] Which teams are using GitLab?
- [ ] What do users say about GitLab?
- [ ] How has GitLab's monthly release cycle benefitted your SDLC?
- [ ] Has the regular release cycle supported innovation and collaboration?
- [ ] If your team is not on the most recent version (or within a release or two), why haven't you updated?

**Impact of GitLab**
- [ ] What are the positive business outcomes of introducing GitLab?
- [ ] What are some initial successes resulting from moving to GitLab?
- [ ] Can you share any larger successes?

### [Command of Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) Questions

**Value Driver 1 - Increasing Operational Efficiencies**
- [ ] Has GitLab simplified workflow?
- [ ] How has GitLab enabled cross-functional relationships?
- [ ] How has it helped modernize architecture?
- [ ] What are some initial successes resulting from moving to GitLab?
- [ ] What does your current workflow look like?
- [ ] What other tools do you have plugged into GitLab?
- [ ] Can you describe how your deployments look?
- [ ] Does GitLab integrate/plug into your use of cloud resources?
- [ ] Have you measured throughput/Lead time? Can you give a before and after?
- [ ] Has there been an impact on change failure rate?
- [ ] Is your MTTR (mean time to resolution) improved?

- [ ] Tell me how Productive the development process was prior to GitLab?
> - [ ] How long to deploy (per day/week/month). After?
> - [ ] Were there issues with deployment/bugs prior to GitLab? After?
> - [ ] How frequently did you deploy (per day/week/month)? After? 
> - [ ] Are you using many tools or integrating? Do you use GitLab as a SSOT?
> - [ ] Do you notice a development team slowdown due to using different tooling?
> - [ ] If you switched from multiple tools to GitLab as a SSOT, do you have specific numbers to validate the time-savings?

- [ ] Describe the development process, cycle time.  
> - [ ] How much time was your developer spending in a cycle prior to GitLab? After?
> - [ ] Are you noticing that compressed cycle time-savings? How much?

- [ ] Let's discuss downtime prior to GitLab. How was it managed?
> - [ ] How long were you down?
> - [ ] What did you do about it?
> - [ ] With GitLab, have you seen an improvement in your time restored? What is it approximately?

- [ ] Regarding Developer's tooling, prior to GitLab, were they all using similar tooling, or their own unique services?
> - [ ] After? what does the tooling set up look like now?

- [ ] What were your ballpark costs for development annually prior to GitLab? After?
> - [ ] User/license costs?
> - [ ] Computer costs?

**Value Driver 2 - Deliver Better Products Faster**
- [ ] Are your processes and tools now more streamlined? Can you describe what the process looks like now?
- [ ] How does having control and visibility of the projects help with planning and governance? 
- [ ] Do your developer teams feel more connected and empowered?
- [ ] How has detecting bugs earlier in the lifecycle impacted the output?

- [ ] Have you noticed a marked impact on your revenue or customer stream/retention since adopting GitLab?
> - [ ] If Yes, what were your numbers for #value prior to GitLab? What does it look like with GitLab?
- [ ] How often did you deploy to staging environments or production enviornments before GitLab?  After?
- [ ] How often, prior to GitLab, did you encounter failures that required remediation after deploying to production?
> - [ ] How long did remediation take on average?
> - [ ] How often, after GitLab, do you encounter failures?
> - [ ] How long do these failures take to remediate?

**Value Driver 3 - Reduce Security and Compliance Risk**
- [ ] Are projects delivering on time and on budget?
- [ ] How has the additional security testing impacted your projects?
- [ ] Are audits quicker and seamless now?

- [ ] How often did you encounter security vulnterabilities in production prior to GitLab?
> - [ ] After GitLab, did you notice an increase of security vulnerability detection prior to production, and a decrease in vulnerabilities once deployed?
> - [ ] Specific before/after numbers? 
> - [ ] What percentage of critical security vulnerabilities that previously escaped development have been caught?

- [ ] How far left did you shift with GitLab?
> - [ ] What was the end result of shifting further left? Quantify fewer vulnerabilities in production? Fewer vulnerabilies for security team to manage?
> - [ ] Did shifting left make security team more productive? How? How much?

- [ ] Were you scanning your code for vulnerabilities prior to GitLab?
> - [ ] If yes, how much % (ballpark if needed)
> - [ ] After GitLab, how much of your code are you scanning
> - [ ] If no and have Ultimate, educate on how to use this feature
- [ ] Before GitLab, how much time did you spend auditing your code?
> - [ ] With GitLab, how much time is spent now on auditing?

- [ ] What other compliance efforts did you use prior to GitLab?
- [ ] After? What compliance efforts did it facilitate?

**Impact metrics of using GitLab**

- [ ] Do you have any metrics available?
- [ ] How has GitLab impacted your cycle time?
- [ ] Have you measured throughput/Lead time? Can you give a before and after?
- [ ] What does your deployment frequency look like?
- [ ] Has there been an impact on change failure rate?
- [ ] Is your MTTR (mean time to resolution) improved?
- [ ] What percentage of your releases are now on time and on budget?
- [ ] Are you catching bugs earlier? Are you catching bugs that would have previously been missed?
- [ ] What percentage of your code is now scanned?
- [ ] What percentage of critical security vulnerabilities that previously escaped development have been caught?
- [ ] Has GitLab helped you achieve the KPIs you set out to solve?

**Customer Use Case Questions**
Depending on the customers use cases; please select the appropriate questions below to ask the customer. 

**Version Control & Collaboration (VC&C)/Source Code Management (SCM)** 
- [ ] How does your team create, manage and protect your project artifacts. (source code, designs, configuration, etc)?
- [ ] How were you managing project assets prior to GitLab?
- [ ] Does having one SCM tool improve your development process (efficiency, speed, quality, etc)?

**Continuous Integration (CI)** 
- [ ] How are you automating software development processes, testing and building code?
- [ ] How does GitLab Continuous Integration add value to your SDLC?
- [ ] How has GitLab Continuous Integration helped improve quality and speed of your delivery? 

**Continuous Delivery (CD)**
- [ ] How are you improving the speed to deploy code?
- [ ] Has using GitLab Continuous Delivery empowered your developers to deploy changes faster?  What's their feedback?
- [ ] Has the automated delivery increased your code output while maintaining quality?

**Development, Security, and Operations (DevSecOps)** 
- [ ] How are you identifying application vulnerabilities during your development process?
- [ ] How has detecting security bugs earlier impacted the SDLC?
- [ ] How has GitLab helped support audits and compliance etc?
- [ ] How has shifting security earlier in the delivery lifecycle impacted your speed to deliver?

**Agile Project Management (Agile)** 
- [ ] How are you managing your team's backlog, sprints, milestones, and future work?
- [ ] How do you have visibility of team results?
- [ ] How do you track/monitor project milestones etc?

**Simplify Development Operations / End to End DevOps (DevOps)** 
- [ ] How has collaboration across teams improved with GitLab?
- [ ] How has productivity or efficiency improved using GitLab? 
- [ ] What other benefits have you realized with GitLab and a single application?

**Cloud Native Approach to Applications Development (CloudNative)**
- [ ] How are you developing and managing cloud native applications? 
> - [ ] K8s executor?
> - [ ] Canary deployments?
> - [ ] AutoDevOps? Using Templates?
> - [ ] CI/CD to orchestrate deployments?
- [ ] How has GitLab helped you to manage cloud native/microservices? 
- [ ] What are the benefits of using GitLab's kubernetes integration?
- [ ] How long have you been using Cloud Native, what has the journey been like?
> - [ ] When did you start adopting K8s? (before/after GitLab?)
- [ ] How many clusters do you have?
- [ ] If Autoscaling - what were you doing before?
> - [ ] How long did it take you? (cost of efficiency, speed, security)

**Infrastructure as Code (GitOps)**
- [ ] How are your operations teams managing different versions of their automation/configuration scripts?
- [ ] How are you testing and validating your infrastructure scripts?
- [ ] How are you integrating with other tools to maintain IAC?

**Deployment Strategy Questions**
- [ ] Can you describe the current deployment strategy you are using with GitLab? (target environment types, on-premise infrastructure and/or public cloud services, some combination of these and/or other resources)?
- [ ] Can you share the background and reasoning behind selecting this current deployment strategy?
- [ ] Were there specific business and/or technical reasons that shaped this strategy?
- [ ] **If customer identified they are using cloud services or plan to** Is your organization consuming or planning to consume cloud services? If so, which ones? and over what timeframe? Can you describe your current cloud deployment architecture-- if applicable?
- [ ] Are you using or planning to use containers? If so, can you describe your use of containers?

**Education Program Customers**
- [ ] How is GitLab advancing research at your University/Research Organization? 	
- [ ] What role does GitLab have in advancing open science?
- [ ] How are you using GitLab in your organization?
- [ ] What do the students and researchers like most about GitLab?
- [ ] What advantages does learning GitLab while in University give to students entering the professional world?
- [ ] How are students and researchers using GitLab to collaborate?

**Open Source Program Customers**
- [ ] How is GitLab impacting the number of contributions in your community?
- [ ] What kind of feedback have you received about GitLab from your community? 
- [ ] Did you need to adapt GitLab to fit your community’s needs? 
- [ ] What are the benefits of moving to GitLab for open source communities?
- [ ] What other services did you use before GitLab? (e.g. for hosting, task management, etc)
- [ ] What other services are you still using? (e.g. for hosting, task management, etc)
- [ ] Did you leverage any other open source programs? (e.g. Packet program, AWS credits, etc)
- [ ] (If not using the Community Edition) Which top tier enterprise features, if any, do you find most helpful? 
- [ ] How are you measuring the value that GitLab is delivering for your open source program?

### Written Case Study creation process

Written Case studies are vital to showcasing the success of our customers and display how they overcame the pain and challenges their organizations were facing in their software development lifecycle. This is an opportunity to describe how GitLab helps overcome these pain points and provide value to the organnization. Often these stories require time for proof points and metrics to be established within customer organization.  Here are the steps to creating written case studies. 

1. Sales Rep follows process outlined in [Reference Program Process](### Reference-Program-Process) to alert the Customer Reference Manager that customer has agreed to participating in written case study process. 
2. Sales Rep introduces CRM to customer by email. CRM coordinates an introduction call with customer.
3. CRM meets with customer for intro call to uncover customer story and to label case study issue with applicable value drivers and customer use case. CRM will share applicable [interview questions](#### Common-Interview-questions) with customer by email. During this session, CRM will work with customer to determine the best timeframe for interview call. Common time frames are 30 days, 60 days, 90 days or 6 months. CRM distributes the logo permission form to customer to help increase speed of legal approval. CRM will also bring up possibility of Media opportunities for case study. 
4. For the interview call, the CRM leads the discussion. The content creator is on the call to make sure questions are answered to get the case study completed and that they have everything they need to create case study. 
5. CRM gets interview transcribed using rev.com and sends the resulting transcript to customer for redlines/changes and approval to use. CRM to check with customer that legal and communications team is aligned with the process and is approving assets along the way. 
6. Once the transcript is approved for use, CRM puts approved transcript in the customer-identified folder in WIP Case Studes in Google Drive. CRM alerts Customer Content that the transcript is approved and links to it in the case study issue description. 
7. Customer Content drafts case study and alerts CRM when draft is ready for review. 
8. CRM reviews draft for completeness, content and makes sure it acurately refects the customer story. CRM brings in designated use case PMM teammember for additional review (if necessary) and the Alliances/Partner team is alerted to draft by alert in the issue if the CRM deems it necessary. The additional reviewers will be tagged in the issue and also slacked the issue. They will have 3 working days to respond.The Draft is sent to PR team for media opportunity exploration. Draft is sent to content team for a copy edit review. 
9. Customer content uses feedback and creates a clean draft to send to customer. CRM sends draft to customer with Customer content cc'd in the email. This email should be sent as both a link to a google doc as well as a .docx file attached to the email. 
10. Once customer feedback is recieved, customer content addresses any concerns and CRM reviews and sends final version to customer for approval or continued discussion around draft. 
11. If the marketing approval form hasn't already been recieved, CRM sends the marketing approval form for the customer to review, sign and return to GitLab. 
12. CRM will review the PR potential of the reference with the PR team and if they deem it has PR merit, the CRM will propose PR support for the reference to the customer.
12. If customer agrees to media awareness around the case study, introduction to the PR team is co-ordinated by the CRM.
13. Once customer approves final version and a signed marketing permission form is recieved, CRM loads the signed form into the SFDC account and either customer content or CRM publishes case study on the /customers page. 
14. CRM and Customer content begin internal promotion process. Raise a Customer Content Activation issue, the CRM is the DRI for this activity. 

Please note that we can facilitate customer interviews in other languages (FR/DE/IT/ES/JP) if the customer prefers this option

#### Publishing to the website

1. Start by opening a public issue and an Merge Request in the www-gitlab-com project.
2. Create a `.yml` file in `/data/case_studies` directory under Marketing site repo (www-gitlab-com project). This can be accomplished in the Web IDE.
3. Keep the name of file same as company name (this is not mandatory but it is easier to manage), for eg; if company name is "Foobar", create a file as `/data/case_studies/foobar.yml`.
4.	Once created, add contents of the file using the sample Case Study template, or by scraping the content from an existing case study, and then update the values of each property based on case study details, remember, do NOT change property names.
5. Add images to the same MR. This ensures they show up in the preview app. To accomplish this, stay in your MR and on the left rail open the selected file. Upload the file to the specific directory by hovering over the file and selecting upload file. The Cover image needs to be a .jpg and put in 'source/image/blogimage' directory. The company logo needs to be a color .SVG image and is placed in the 'source/image/case_study_logos' directory. 
6.	In the Web IDE, you can view generated Case Study page in the generated App under the URL, for eg; http://localhost:4567/customers/foobar.
7. Once the case study is ready to publish, remove the WIP label from your MR and ask an approved publisher to post. 

#### Adding customer logo and case study to customer's grid

1. When a customer has approved the usage of their logo on our /customers page, we place their approved logo in the grid on the bottom of the page.
2. Open an MR in the www-gitlab-com project. 
3. Add the approved single-tone .svg logo to the /source/images/organizations file.
4. Once the .svg is live in the organizations file, open the organizations.yml at www-gitlab-com in the /data file
5. Add new lines in the file to correspond to the exsiting format. 
6. Ensure the asset_type and asset_link are filled in and point to the live case study. 
7. Once the preview app shows a successful addition, remove the WIP label from your MR and ask an approved publisher to post. 

