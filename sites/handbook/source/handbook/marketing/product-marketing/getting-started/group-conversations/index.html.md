---
layout: markdown_page
title: "Strategic Marketing Group Conversations How-to"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is a Group Conversation (GC)?
GitLab departments update the rest of the company on what they are achieving and working on through [Group Conversations](/handbook/people-group/group-conversations/). Group Conversations are an important way to make sure the rest of the organization is aware of what each team is up to, has an opportunity to ask questions, and have a collection of links and information for reference.

## Old GC decks
The Strategic Marketing department (formerly known as Product Marketing) previous decks are in our [Google drive](https://drive.google.com/drive/u/0/folders/1fCEAj1HCegJOJE_haBqxcy2NYm0DS1FO) (GitLab employees only). It's useful when putting together the next GC deck to look at the last one to make sure that items on the calendar edge don't get missed or included a second time.

## Putting together the content
We almost always know when our next GC is and create the deck for it as soon as we finish the last one. To find the latest Next GC deck we are working on, look at the #pmmteam Slack channel topic.

### What's new section
1. We should all be recording info in the next GC deck as we complete them (daily, weekly, etc). The goal is to not have to go back through our calendars, emails, slack messages, meeting notes, etc right before the GC session to dig up the past. This is harder to do and more likely we will miss items.
1. Add ***accomplishments/results***, not "what we are doing", "what we are working on", etc
1. Add your contents onto the slide that is marked for your team (eg. Technical Marketing).
1. ***SUPER IMPORTANT: Make sure*** that the content you are adding wasn't already identified in a previous GC.
1. As many items as possible should have links (either to the deliverable itself, the video, the presentation, the pointer to the conference speaker's session, etc). Do your best to link to something relevant.
1. Be conscious of formatting. When you add content, if you are pasting it in, use "Paste without formatting" (on Mac: command-shift-V) to pick up the formatting and text sizing that is already on the page.
1. Clarity through brevity. Describe what the accomplishment/result is in as concise and brief a way as possible (with the exception of long formal titles. those should be left as is)
1. Pictures or screenshots that relate to some of your content are good to add. This doesn't need to be boring.

### What's next section
1. There is always at least a slide or two which collects what each team is expecting to deliver within the next GC period (about every 6 weeks). Find the slide with your team listed, and add what you expect to deliver within the next period.
1. Team managers will consolidate to top 3 for team on top 3 slide.
1. Make sure to add a link for as many items as possible (at this stage usually a GitLab Issue or MR). This enables others to contribute to what you are working on while it is still in flight.
1. As much as possible, put the expected delivery date (at least month) in parenthesis after your item so others know what the current plan is (don't worry, this does not set it in stone).
