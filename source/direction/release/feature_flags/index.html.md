---
layout: markdown_page
title: "Category Direction - Feature Flags"
description: Feature Flags can be used as part of software development to enable a feature to be tested even before it is completed and ready for release. Learn more!
canonical_path: "/direction/release/feature_flags/"
---

- TOC
{:toc}

## Feature Flags

Feature Flags can be used as part of software development to enable a feature to be tested even before it is completed and ready for release. 
A feature flag is used to enable or disable the feature during run time. 
The unfinished features are hidden (toggled) so they do not appear in the user interface. 

Tying into our [progressive delivery](https://about.gitlab.com/direction/ops/#progressive-delivery) strategy, this allows many small incremental versions of software to be delivered without the cost of constant branching and merging. 

Our ultimate goal is to provide an easy way to configure and monitor feature flags that integrate into the continuous development cycle. Visual ties from feature flags to ongoing issues and merge requests will help understand the state
and exposure of each feature and a comprehensive dashboard will allow developers to take action when necessary in order to minimize risk and technical debt. 

Feature Flags is built with an [Unleash](https://github.com/Unleash/unleash)-compatible
API, ensuring interoperability with any other compatible tooling,
and taking advantage of the various client libraries available for
Unleash. Unleash have recently announced that they are spinning up a hosted (paid) option while maintaining their open source offering. We will be monitoring this closely. 

While Feature flags are a great tool for incremental delivery, they also introduce technical debt, sometimes feature flags are forgotten and left as stale code.
- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AFeature%20Flags)
- [Overall Vision](/direction/ops/#release)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Arelease&label_name[]=Category%3AFeature%20Flags)
- [Documentation](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)
- [Deep Dive video](https://www.youtube.com/watch?v=wrbfyTtDA8w&feature=youtu.be)

## What's Next & Why

In order to expand the users who can use Feature Flags, we are moving Feature Flags to core via [gitlab#212318]https://gitlab.com/gitlab-org/gitlab/-/issues/212318. We are excited about enabling this for everyone and look forward to hearing your feedback and experience.  

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).

Our focus at the moment is on using the feature internally to deliver GitLab itself. This will be driving new requirements to the base features that are out there already, and also helping us to ensure the list for `complete` maturity is accurate. Our plan is for our feature flag solution to compete with other products on the market, such as [LaunchDarkly](https://launchdarkly.com/) or [Rollout](https://rollout.io/). As we work towards `complete` maturity, we expect that our primary adopters of this feature will be pre-existing GitLab users looking for incremental value. For buyers who are considering replacing other tools, and looking for something that integrates feature flags with issues, we will also provide a valuable solution as we head towards `complete` maturity.

Key deliverables to achieve this are:

- [Feature Flag Strategies](https://gitlab.com/groups/gitlab-org/-/epics/3978)
  - [Multiple strategies per Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/35554) (Complete)
  - [Feature Flag Gradual Rollout strategy based on lists](https://gitlab.com/gitlab-org/gitlab/issues/13308) (Complete)
  - [Add Rule based Feature Flag rollout strategy support](https://gitlab.com/gitlab-org/gitlab/issues/33315)
  - [Ability to exclude Feature flags for specific users](https://gitlab.com/gitlab-org/gitlab/-/issues/14667)

- [Feature Flag Context within GitLab](https://gitlab.com/groups/gitlab-org/-/epics/3977)
  - [Add ability to associate feature flag with contextual issue](https://gitlab.com/gitlab-org/gitlab/-/issues/26456) (Complete)
  - [Add ability to associate feature flag with contextual Merge Request](https://gitlab.com/gitlab-org/gitlab/-/issues/33615)
  - [Add ability to associate feature flags with contextual epic](https://gitlab.com/gitlab-org/gitlab/-/issues/33578)
  - [Feature Flag contextual code references](https://gitlab.com/gitlab-org/gitlab/-/issues/238540)

- [Get Feature Flags to Enterprise Grade](https://gitlab.com/groups/gitlab-org/-/epics/3976)
  - [Add feature flag permissions](https://gitlab.com/gitlab-org/gitlab/issues/8239)
  - [Capture Feature Flag Toggle actions in the audit log](https://gitlab.com/gitlab-org/gitlab/-/issues/224953)
  - [Create "Protected" Feature Flag strategies](https://gitlab.com/gitlab-org/gitlab/-/issues/230614)
  - [Create Best Practice Guide for using Feature Flags](https://gitlab.com/gitlab-org/gitlab/-/issues/230629)

- [Move features to core: "Feature Flags"](https://gitlab.com/gitlab-org/gitlab/-/issues/212318)
- [A/B testing based on Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/34813)

## Competitive Landscape

Other feature flag products offer more comprehensive targeting and
configuration. The simplicity of our solution is actually a strength
compared to this in some cases, but there is some basic functionality
still to add. As we are rigorously working to close the gaps with the competitors, our next strategy to tackle will be the ability to configure feature flags based on groups [gitlab-ee#13308](https://gitlab.com/gitlab-org/gitlab-ee/issues/13308)

A competitor review of LaunchDarkly can be found in  [gitlab&4102](https://gitlab.com/groups/gitlab-org/-/epics/4102). If you have additional 
insights or are interested in joining in the conversation, please comment on the issue. 

## Analyst Landscape

Analysts are recognizing that this sort of capability is becoming
more a part of what's fundamentally needed for a continuous delivery
platform, in order to minimize blast radius from changes. Often,
solutions in this space are complex and hard to get up and running
with, and they are not typically bundled or well integrated with CD
solutions. 

This backs up our desire to not over complicated the solution space
here, and highlights the need for guidance. [gitlab#9450](https://gitlab.com/gitlab-org/gitlab/issues/9450)
introduces new in-product documentation to help development and
operations teams learn how to successfully adopt feature flags.

## Top Customer Success/Sales Issue(s)

Our top customer success issue, and one that comes up frequently in customer calls is [gitlab#8239](https://gitlab.com/gitlab-org/gitlab/-/issues/8239) which talks about feature flag permissions. This will explicitly give permissions to toggle feature flags on/off based on the environment. 

## Top Customer Issue(s)

Our most popular customer issue is to open-source Feature Flags via [gitlab#212318](https://gitlab.com/gitlab-org/gitlab/-/issues/212318).

## Top Internal Customer Issue(s)

Adding a webhook for feature flag events via [gitlab#220898](https://gitlab.com/gitlab-org/gitlab/-/issues/220898) to trigger pipeline actions and even email/slack notifications in case a flag was toggled. This can be really useful for automation, and to help keep you informed on the feature flag status at all times.

### Dogfooding Efforts

We have completed the re-architecture of Feature Flags, adding the ability to support multiple strategies per environment via API and UI ([gitlab#204895](https://gitlab.com/gitlab-org/gitlab/issues/204895 and [gitlab#35555](https://gitlab.com/gitlab-org/gitlab/issues/35555)). Our Fulfillment, Expansion, and Telemetry teams have joined the dogfooding effort for this. The feedback from this process is being tracked in [gitlab&3760](https://gitlab.com/groups/gitlab-org/-/epics/3760).

## Top Vision Item(s)

One of our main themes in CI/CD is [Progressive delivery](https://about.gitlab.com/direction/ops/#progressive-delivery). Feature flags, by definition, are a form of progressive delivery as they allow you to deploy code incrementally and control the audience that will receive the new code. Our top vision issues are connecting Feature Flags to issues, Merge Requests, and Epics so that our users can benefit from our single application toolchain. This will enable users to better understand the context of a feature flag and their state to the associated plan, release, and deployment.

